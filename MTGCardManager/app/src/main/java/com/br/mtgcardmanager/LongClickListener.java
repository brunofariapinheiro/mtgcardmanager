package com.br.mtgcardmanager;

/**
 * Created by Bruno on 19/08/2016.
 */
public interface LongClickListener {

    void onItemLongClick(int position);

}
